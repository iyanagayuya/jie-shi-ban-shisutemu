package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = {"/signup"})
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		List<String> messages = new ArrayList<String>();

		HttpSession session = request.getSession();
		if (isValid(request, messages) == true) {
			User user = new User();
			user.setLoginId(request.getParameter("login_id"));
			user.setPassword(request.getParameter("password"));
			user.setName(request.getParameter("name"));
			user.setBranch(Integer.parseInt(request.getParameter("branch")));
			user.setDepartment(Integer.parseInt(request.getParameter("department")));

			new UserService().register(user);
			response.sendRedirect("./");
		} else {
			session.setAttribute("errorMessages", messages);
			User user = new User();
			user.setLoginId(request.getParameter("login_id"));
			user.setName(request.getParameter("name"));
			user.setBranch(Integer.parseInt(request.getParameter("branch")));
			user.setDepartment(Integer.parseInt(request.getParameter("department")));
			request.setAttribute("user", user);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String loginId = request.getParameter("login_id");
		String password = request.getParameter("password");
		String repassword = request.getParameter("repassword");
		String name = request.getParameter("name");

		if (StringUtils.isEmpty(loginId)) {
			messages.add("IDを入力してください");

		}
		else if (!(loginId.matches("[a-zA-Z0-9]{6,20}"))) {
			messages.add("半角英数字で6文字以上20文字以下で入力してください");
		}
		if (StringUtils.isEmpty(password)) {
			messages.add("パスワードを入力してください");
		}
		else if (!(password.matches("[ -~｡-ﾟ]{6,20}"))) {
			messages.add("半角文字で6文字以上20文字以下で入力してください");
		}
		else if (!(repassword.equals(password))) {
			messages.add("パスワードが一致してません");
		}
		if (StringUtils.isEmpty(name) == true) {
			messages.add("名称を入力してください");
		}
		else if (StringUtils.length(name) >10) {
			messages.add("10文字以下で入力してください");
		}

		//重複エラー確認
		User loginUser = new UserService().getLoginId(loginId);
		if(loginUser != null) {
			messages.add("このログインIDは既に登録済みです");
		}
		// TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
