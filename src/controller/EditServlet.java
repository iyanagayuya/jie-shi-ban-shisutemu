package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import service.EditService;

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		int Id = Integer.parseInt(request.getParameter("id"));
		User editUser = new EditService().getUser(Id);
		request.setAttribute("editUser", editUser);
		request.getRequestDispatcher("edit.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		User editUser = getEditUser(request);

		if (isValid(request, messages) == true) {
			try {
				new EditService().update(editUser);
			} catch (NoRowsUpdatedRuntimeException e) {
				messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
				session.setAttribute("errorMessages", messages);
				request.setAttribute("editUser", editUser);
				request.getRequestDispatcher("edit.jsp").forward(request, response);
				return;
			}

			session.setAttribute("id", editUser);

			response.sendRedirect("./");
		} else {
			session.setAttribute("errorMessages", messages);
			request.setAttribute("editUser", editUser);
			request.getRequestDispatcher("edit.jsp").forward(request, response);
		}
	}

	private User getEditUser(HttpServletRequest request)
			throws IOException, ServletException {

		User editUser = new User();
		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setLoginId(request.getParameter("login_id"));
		editUser.setPassword(request.getParameter("password"));
		editUser.setName(request.getParameter("name"));
		editUser.setBranch(Integer.parseInt(request.getParameter("branch")));
		editUser.setDepartment(Integer.parseInt(request.getParameter("department")));
		return editUser;
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		int id = Integer.parseInt(request.getParameter("id"));
		String login_id = request.getParameter("login_id");
		String password = request.getParameter("password");
		String repassword = request.getParameter("repassword");
		String name = request.getParameter("name");

		if (StringUtils.isEmpty(login_id)) {
			messages.add("ログインIDを入力してください");
		} else if (!(login_id.matches("[a-zA-Z0-9]{6,20}"))) {
			messages.add("半角英数字で6文字以上20文字以下で入力してください");
		}
		if (!(StringUtils.isEmpty(password) && StringUtils.isEmpty(repassword))) {
			if (!(password.matches("[ -~｡-ﾟ]{6,20}"))) {
				messages.add("半角文字で6文字以上20文字以下で入力してください");
			} else if (!(password.equals(repassword))) {
				messages.add("パスワードが一致してません");
			}
		}
		if (StringUtils.isEmpty(name)) {
			messages.add("名前を入力してください");
		} else if (StringUtils.length(name) > 10) {
			messages.add("10文字以下で入力してください");

		}
		User loginUser = new EditService().getEditId(login_id, id);
		if (loginUser != null) {
			messages.add("このログインIDは既に登録済みです");
		}
		// TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
		if (messages.size() == 0) {
			return true;
		} else
			return false;
	}
}