package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import beans.UserInfo;
import service.GetUserInfoService;
import service.UserService;


@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	List<UserInfo> users = new GetUserInfoService().getUser();

        request.setAttribute("registerUser", users);
        request.getRequestDispatcher("/home.jsp").forward(request, response);
    }

    @Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

    	User user = new User();
    	user.setId(Integer.parseInt(request.getParameter("id")));
    	user.setStatus(Integer.parseInt(request.getParameter("status")));

    	UserService service = new UserService();
    	service.changeStatus(user);


    	response.sendRedirect("./");
    }
}