package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import beans.User;
import dao.ConfirmDao;
import dao.EditStatusDao;
import dao.UserDao;
import utils.CipherUtil;

public class UserService {

	public void register(User user) {

		Connection connection = null;
		try {
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			UserDao userDao = new UserDao();
			userDao.insert(connection, user);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	public void changeStatus(User user) {

		Connection connection = null;
		try {
			connection = getConnection();

			EditStatusDao editstatusDao = new EditStatusDao();
			editstatusDao.update(connection, user);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	public User getLoginId(String loginId) {

		Connection connection = null;
		try {
			connection = getConnection();

			ConfirmDao confirmDao = new ConfirmDao();
			User ret = confirmDao.check(connection, loginId);

			commit(connection);
			return ret;
		} catch (RuntimeException e) {
			rollback(connection);

			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}