package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import org.apache.commons.lang.StringUtils;

import beans.User;
import dao.ConfirmDao;
import dao.EditDao;
import utils.CipherUtil;

public class EditService {

	public User getUser(int id) {

		Connection connection = null;
		try {
			connection = getConnection();

			EditDao editDao = new EditDao();
			User ret = editDao.getUser(connection, id);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void update(User user) {

		Connection connection = null;
		try {
			connection = getConnection();
			if(!StringUtils.isEmpty(user.getPassword())) {
			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);
		}

			EditDao editDao = new EditDao();
			editDao.update(connection, user);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	public User getEditId(String loginId, int id) {

		Connection connection = null;
		try {
			connection = getConnection();

			ConfirmDao confirmDao = new ConfirmDao();
			User ret = confirmDao.search(connection, loginId, id);

			commit(connection);
			return ret;
		} catch (RuntimeException e) {
			rollback(connection);

			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}