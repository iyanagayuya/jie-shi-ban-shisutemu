package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.UserInfo;
import dao.GetUserInfoDao;

public class GetUserInfoService {

	public List<UserInfo> getUser() {

		Connection connection = null;
		try {
			connection = getConnection();

			GetUserInfoDao userDao = new GetUserInfoDao();
			List<UserInfo> ret = userDao.getUserInfo(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}