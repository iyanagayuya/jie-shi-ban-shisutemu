package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import beans.User;
import exception.SQLRuntimeException;

public class ConfirmDao {

	public User check(Connection connection, String loginId) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM users WHERE login_id = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, loginId);

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("id");
				String login_id = rs.getString("login_id");
				String name = rs.getString("name");
				int branch = rs.getInt("branch");
				int department = rs.getInt("department");

				User loginUser = new User();
				loginUser.setId(id);
				loginUser.setLoginId(login_id);
				loginUser.setName(name);
				loginUser.setBranch(branch);
				loginUser.setDepartment(department);
				return loginUser;
			}
			return null;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public User search(Connection connection, String loginId, int id) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM users WHERE login_id = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, loginId);


			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				int userId = rs.getInt("id");
				String login_id = rs.getString("login_id");
				String name = rs.getString("name");
				int branch = rs.getInt("branch");
				int department = rs.getInt("department");
				if (userId == id) {
					return null;
				}
					User loginUser = new User();
					loginUser.setId(userId);
					loginUser.setLoginId(login_id);
					loginUser.setName(name);
					loginUser.setBranch(branch);
					loginUser.setDepartment(department);
					return loginUser;
			}
			return null;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}