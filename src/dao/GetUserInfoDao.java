package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.UserInfo;
import exception.SQLRuntimeException;

public class GetUserInfoDao {

    public List<UserInfo> getUserInfo(Connection connection ) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("users.id, ");
            sql.append("users.login_id, ");
            sql.append("users.name, ");
            sql.append("branches.branch_name, ");
            sql.append("departments.department_name, ");
            sql.append("users.status ");
            sql.append("FROM users ");
            sql.append("INNER JOIN branches ");
            sql.append("ON users.branch = branches.id ");
            sql.append("INNER JOIN departments ");
            sql.append("ON users.department = departments.id ");
            sql.append("ORDER BY users.id asc" );

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserInfo> ret = toUserInfoList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserInfo> toUserInfoList(ResultSet rs)
            throws SQLException {

        List<UserInfo> ret = new ArrayList<UserInfo>();
        try {
            while (rs.next()) {
            	int id = rs.getInt("id");
                String login_id = rs.getString("login_id");
                String name = rs.getString("name");
                String branch = rs.getString("branch_name");
                String department = rs.getString("department_name");
                int status = rs.getInt("status");

                UserInfo user = new UserInfo();
                user.setId(id);
                user.setLoginId(login_id);
                user.setName(name);
                user.setBranchName(branch);
                user.setDepartmentName(department);
                user.setStatus(status);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}