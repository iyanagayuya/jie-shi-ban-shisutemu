package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.lang.StringUtils;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class EditDao {

	public User getUser(Connection connection, int id) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("users.id, ");
			sql.append("users.login_id, ");
			sql.append("users.name, ");
			sql.append("users.branch, ");
			sql.append("users.department ");
			sql.append("FROM users ");
			sql.append("Where id =" + id);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			User ret = toUser(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private User toUser(ResultSet rs)
			throws SQLException {

		User ret = new User();
		while (rs.next()) {
			int id = rs.getInt("id");
			String login_id = rs.getString("login_id");
			String name = rs.getString("name");
			int branch = rs.getInt("branch");
			int department = rs.getInt("department");

			User editUser = new User();
			editUser.setId(id);
			editUser.setLoginId(login_id);
			editUser.setName(name);
			editUser.setBranch(branch);
			editUser.setDepartment(department);
			return editUser;
		}
		return ret;
	}

	public void update(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append("  login_id = ?");
			sql.append(", name = ?");
			sql.append(", branch = ?");
			sql.append(", department = ?");

			if (!StringUtils.isEmpty("password")) {
				sql.append(", password = ?");
			}
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, user.getLoginId());
			ps.setString(2, user.getName());
			ps.setInt(3, user.getBranch());
			ps.setInt(4, user.getDepartment());
			if (!StringUtils.isEmpty("password")) {
				ps.setString(5, user.getPassword());
				ps.setInt(6, user.getId());
			} else {
				ps.setInt(5, user.getId());
			}

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}
}
