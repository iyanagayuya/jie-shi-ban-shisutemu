package beans;

import java.io.Serializable;

public class UserInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String login_id;
    private String name;
    private String branch;
    private String department;
    private int status;

	public int getId() {
		return id;
	}
	public void setId(int id) {

		this.id = id;
	}
	public String getLoginId() {
		return login_id;
	}
	public void setLoginId(String login_id) {
		this.login_id = login_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBranchName() {
		return branch;
	}
	public void setBranchName(String branchName) {
		this.branch = branchName;
	}
	public String getDepartmentName() {
		return department;
	}
	public void setDepartmentName(String departmentName) {
		this.department = departmentName;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

}