
<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset="UTF-8">
<title>ホーム</title>
</head>
<body>
	<div class="main-content">
		<div class="header">
			<a href="signup">新規登録</a>
			<div class="users">
				<table border="1">
					<tr>
						<th>ログインID</th>
						<th>名称</th>
						<th>支店</th>
						<th>部署・役職</th>
						<th>環境設定</th>
					</tr>
					<c:forEach items="${registerUser}" var="user">
						<div class="user-info">
							<tr>
								<td class="login_id"><c:out value="${user.loginId}" /></td>
								<td class="name"><c:out value="${user.name}" /></td>
								<td class="branch"><c:out value="${user.branchName}" /></td>
								<td class="department"><c:out
										value="${user.departmentName}" /></td>
								<td class="status">
									<form method="post">
										<c:choose>
											<c:when test="${user.status == 1}">
												<input name="id" value="${user.id}" type="hidden">
												<input name="status" value="0"  type="hidden" />
												<input type="submit" value="停止" onClick="return confirm('停止します')" />
											</c:when>
											<c:when test="${user.status == 0}">
												<input name="id" value="${user.id}" type="hidden">
												<input name="status" value="1"  type="hidden" />
												<input type="submit" value="復活" onClick="return confirm('復活します')" />
											</c:when>
										</c:choose>
									</form>
								</td>
								<td class="edit">
									<form action="edit" method="get">
										<input name="id" value="${user.id}" id="id" type="hidden" />
										<input type="submit" value="編集">
									</form>
								</td>
							</tr>
						</div>
					</c:forEach>
				</table>
			</div>
		</div>
	</div>
	<div class="copyright">Copyright(c)iyanaga_yuya</div>
</body>
</html>