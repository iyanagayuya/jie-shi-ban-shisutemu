<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー登録</title>
</head>
<body>
	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<form action="signup" method="post">
			<label for="login_id">ログインID</label><br/>
			<input name="login_id" id="login_id" value = "${user.loginId}" type = "text" placeholder="(半角英数字6文字以上20文字以下)" /> <br />

			<label for="password">パスワード</label><br/>
			<input name="password" type="password" id="password" type = "text" placeholder="(半角文字6文字以上20文字以下)" /> <br />

			<label for="password">  （確認用）</label><br/>
			<input name="repassword" type="password" id="repassword" /> <br />

			<label for="name">名前</label><br/>
			<input name="name" id="name" value = "${user.name}" type = "text" placeholder="(10文字以下)"/><br />

			<label for="branch">支店</label><br>
			<c:choose>
				<c:when test="${user.branch == null}">
					<select name="branch">
						<option value="1">本社</option>
						<option value="2">支店A</option>
						<option value="3">支店B</option>
						<option value="4">支店C</option>
					</select>
				</c:when>

				<c:when test="${user.branch == 1}">
					<select name="branch">
						<option value="1" selected>本社</option>
						<option value="2">支店A</option>
						<option value="3">支店B</option>
						<option value="4">支店C</option>
					</select>
				</c:when>

				<c:when test="${user.branch == 2}">
					<select name="branch">
						<option value="1">本社</option>
						<option value="2" selected>支店A</option>
						<option value="3">支店B</option>
						<option value="4">支店C</option>
					</select>
				</c:when>

				<c:when test="${user.branch == 3}">
					<select name="branch">
						<option value="1">本社</option>
						<option value="2">支店A</option>
						<option value="3" selected>支店B</option>
						<option value="4">支店C</option>
					</select>
				</c:when>

				<c:when test="${user.branch == 4}">
					<select name="branch">
						<option value="1">本社</option>
						<option value="2">支店A</option>
						<option value="3">支店B</option>
						<option value="4" selected>支店C</option>
					</select>
				</c:when>
			</c:choose><br>

			<label for="department">部署・役職</label><br />
			<c:choose>
				<c:when test="${user.department == null}">
					<select name="department">
						<option value="1">総務人事担当者</option>
						<option value="2">情報管理担当者</option>
						<option value="3">支店長</option>
						<option value="4">社員</option>
					</select>
				</c:when>

				<c:when test="${user.department == 1}">
					<select name="department">
						<option value="1" selected>総務人事担当者</option>
						<option value="2">情報管理担当者</option>
						<option value="3">支店長</option>
						<option value="4">社員</option>
					</select>
				</c:when>

				<c:when test="${user.department == 2}">
					<select name="department">
						<option value="1">総務人事担当者</option>
						<option value="2" selected>情報管理担当者</option>
						<option value="3">支店長</option>
						<option value="4">社員</option>
					</select>
				</c:when>

				<c:when test="${user.department == 3}">
					<select name="department">
						<option value="1">総務人事担当者</option>
						<option value="2">情報管理担当者</option>
						<option value="3" selected>支店長</option>
						<option value="4">社員</option>
					</select>
				</c:when>

				<c:when test="${user.department == 4}">
					<select name="department">
						<option value="1">総務人事担当者</option>
						<option value="2">情報管理担当者</option>
						<option value="3">支店長</option>
						<option value="4" selected>社員</option>
					</select>
				</c:when>
			</c:choose><br>
			<input type="submit" value="登録" />
			<br /><a href="./">戻る</a>
		</form>
	</div>
</body>
</html>